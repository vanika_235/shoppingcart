/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;


public class DiscountFactory {
    private static DiscountFactory instance = null;
    
    private DiscountFactory( ) {
        
    }
    
    public Discount getDiscount( Types type ) {
        switch ( type ) {
            case   DISCOUNTPERCENTAGE : return new DiscountByAmount( );
            case DISCOUNTAMOUNT : return new DiscountByPercentage( );
        }
        return null;
    }
    
    
    public static DiscountFactory getInstance( ) {
        if ( instance == null ) {
            instance = new DiscountFactory( );
        }
        return instance;
    }
}




