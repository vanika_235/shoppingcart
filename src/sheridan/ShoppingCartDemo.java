/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author HP
 */
public class ShoppingCartDemo {
  
    public static void main( String args [ ] ) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance( );
        PaymentService creditService = factory.getPaymentService( PaymentServiceType.CREDIT );
        PaymentService debitService = factory.getPaymentService( PaymentServiceType.DEBIT );        
            // create cart and add products
            
        DiscountFactory factory = DiscountFactory.getInstance( );
        Discount discountPercentage = factory.getDiscountPercentage( Types.DISCOUNTPERCENTAGE );
        Discount discountAmount = factory.getDiscountAmount( Types.DISCOUNTAMOUNT );        
        Cart cart = new Cart( );
        cart.addProduct( new Product( "shirt" , 50 ) );
        cart.addProduct( new Product( "pants" , 60 ) );
            // set credit service and pay
        cart.setPaymentService( creditService );        
        cart.payCart();
            // set debit service and pay
        cart.setPaymentService( debitService );    
        cart.payCart(); 
        
        cart.setDiscountPercentage(discountPercentage);
        cart.payCart();
        cart.setDiscountAmount(discount Amount );
        cart.payCart();
        
        
    } 
    
}


